:BOOKID: release-notes
:BZURL: https://gitlab.com/fedora/docs/fedora-linux-documentation/release-notes/-/issues
// Change this to the latest version
:COMMONBUGS_URL: https://discussion.fedoraproject.org/tags/c/ask/common-issues/82/all/f40
:HOLDER: Fedora Project Contributors
:PREVVER: 39
:PRODUCT: Fedora Documentation
:PRODVER: rawhide
:YEAR: 2024
